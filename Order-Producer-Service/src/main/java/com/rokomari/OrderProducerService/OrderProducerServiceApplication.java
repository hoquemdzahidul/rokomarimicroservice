package com.rokomari.OrderProducerService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class OrderProducerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderProducerServiceApplication.class, args);
	}

}
