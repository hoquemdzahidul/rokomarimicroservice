package com.rokomari.OrderProducerService.Service;

import com.rokomari.OrderProducerService.dto.Order;
import com.rokomari.OrderProducerService.utils.ApplicationKey;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OrderProducerService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sentOrderToOrderInsertionService(Order order)
    {
        log.info("sentOrderToOrderInsertionService Method inside OrderProducerService ");
        log.info("order Object : " + order.toString());
        rabbitTemplate.convertAndSend(ApplicationKey.ORDER_SERVICE_EXCHANGE, ApplicationKey.ORDER_SERVICE_ROUTING_KEY, order);

    }
}
