package com.rokomari.OrderProducerService.Controller;

import com.rokomari.OrderProducerService.Service.OrderProducerService;
import com.rokomari.OrderProducerService.dto.Order;
import com.rokomari.OrderProducerService.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping(value = "/order")
@Slf4j
public class OrderProducerController {

    @Autowired
    private OrderProducerService orderProducerService;

    @PostMapping(value = "/create-order")
    public String CreateOrder(@RequestBody Order order)
    {

        order.setOrderId(Utils.orderIdGeneratro());

        log.info("create order method inside Order producer controller...");

        orderProducerService.sentOrderToOrderInsertionService(order);
        return order.getOrderId();
    }

}
