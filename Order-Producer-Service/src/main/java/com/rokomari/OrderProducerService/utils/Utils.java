package com.rokomari.OrderProducerService.utils;

import java.util.UUID;

public class Utils {
    public static String orderIdGeneratro()
    {
        String currentTimeStampInMilliSeconds = String.valueOf(System.currentTimeMillis());
        return UUID.randomUUID().toString() + currentTimeStampInMilliSeconds;
    }
}
