package com.rokomari.order.processing.service.utils;

public class ApplicationKey {
    public static final String ORDER_SERVICE_QUEUE_NAME= "ROKOMARI_OrderService_Order_QUEUE";
    public static final String ORDER_SERVICE_EXCHANGE= "ROKOMARI_OrderService_Order_EXCHANGE";
    public static final String ORDER_SERVICE_ROUTING_KEY= "ROKOMARI_OrderService_Order_ROUTING_KEY";
}
