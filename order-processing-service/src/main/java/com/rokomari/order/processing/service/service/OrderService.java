package com.rokomari.order.processing.service.service;

import com.rokomari.order.processing.service.entity.OrderInfo;
import com.rokomari.order.processing.service.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    public void createOrder(OrderInfo order)
    {
        log.info("Inside Create Order Method inside Order Repository");
        orderRepository.save(order);

    }
}
