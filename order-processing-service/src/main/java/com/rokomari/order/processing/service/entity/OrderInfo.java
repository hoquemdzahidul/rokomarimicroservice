package com.rokomari.order.processing.service.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class OrderInfo {

    @Id
    private String orderId;
    @Column(name = "order_date")
    private Date orderDate;
    @Column(name = "customer_email" )
    private String customerEmail;
    @Column(name = "product_quantity")
    private int quantity;
   @Column(name = "product_price")
    private double productPrice;

}
