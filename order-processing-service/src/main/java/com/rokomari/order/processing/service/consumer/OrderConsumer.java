package com.rokomari.order.processing.service.consumer;

import com.rokomari.order.processing.service.entity.OrderInfo;
import com.rokomari.order.processing.service.service.OrderService;
import com.rokomari.order.processing.service.utils.ApplicationKey;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OrderConsumer {
    @Autowired
   private OrderService orderService;

    @RabbitListener(queues = ApplicationKey.ORDER_SERVICE_QUEUE_NAME)
    public void ConsumeMessageFromRabbitMqQueue(OrderInfo order)
    {

        log.info("COnsume rokomari  message from rabbitMQ Zahid : " + order);
        orderService.createOrder(order);
    }
}
