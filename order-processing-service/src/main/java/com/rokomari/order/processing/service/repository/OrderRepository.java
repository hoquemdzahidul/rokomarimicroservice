package com.rokomari.order.processing.service.repository;


import com.rokomari.order.processing.service.entity.OrderInfo;
import org.springframework.data.repository.CrudRepository;

//@Repository
public interface OrderRepository extends CrudRepository<OrderInfo, String> {
}
